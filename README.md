# watchtower

A Go service framework which aims to remove some boilerplate in a multi-environment way, and can be built upon within an environment for multi-service reuse.

Responsible for:
- Network listeners
- Basic HTTP routing as required
- Passing loaded configuration and network to dependency stack initialisation
- Startup/graceful shutdown triggers and passing those events to custom submodules