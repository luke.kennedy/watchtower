package watchtower

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"runtime/debug"
)

type HTTPModule[TConfig any, TDeps DependencyStack[TConfig, TStdLib], TStdLib StdLib] interface {
	Module[TConfig, TDeps, TStdLib]
	AddRoutes(ctx context.Context, mux *http.ServeMux) error
}

type HTTPMiddleware interface {
	Wrap(next http.Handler) http.Handler
}

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) configureHTTP(ctx context.Context, cfg TConfig) {
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Configuring HTTP server...")
	addr := fmt.Sprintf("%s:%d", cfg.GetAddress(), cfg.GetPort())
	mux := &http.ServeMux{}
	s.mux = mux
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Adding routes from HTTP Modules...")
	for _, module := range s.HTTPModules {
		module.AddRoutes(ctx, mux)
	}
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Checking HTTPS requirements...")
	var gotTLSConfig *tls.Config
	secure := !cfg.GetHTTPInsecure()
	if secure {
		s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Configuring HTTPS...")
		gotTLSConfig = cfg.GetTLSConfig()
	}
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Creating server...")
	s.mx.Lock()
	defer s.mx.Unlock()
	s.addr = addr
	s.server = &http.Server{
		Addr: s.addr,
		BaseContext: func(l net.Listener) context.Context {
			return s.ongoing.ctx
		},
		ConnContext: func(ctx context.Context, c net.Conn) context.Context {
			return ctx
		},
		Handler: http.HandlerFunc(s.middleware),
	}
	if s.TopLevelMiddleware != nil {
		s.server.Handler = s.TopLevelMiddleware.Wrap(http.HandlerFunc(s.middleware))
	}
	if secure {
		s.server.TLSConfig = gotTLSConfig
	}
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "HTTP configuration complete")
}

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) middleware(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer func() {
		if r := recover(); r != nil {
			s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Caught panic in HTTP middleware: %v", r))
			s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Panic trace:\n%s", debug.Stack()))
		}
	}()
	ctx = r.Context()
	path := r.URL.Path
	func() {
		for _, unloggedPath := range s.UnloggedPaths {
			if path == unloggedPath {
				// No logging this
				return
			}
		}
		for _, rx := range s.UnloggedPathsRegexp {
			if rx.Match([]byte(path)) {
				// No logging this
				return
			}
		}
		s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Received HTTP request: %s %s", r.Method, r.URL.Path))
	}()
	// Continue with normal routes
	s.mux.ServeHTTP(w, r)
}
