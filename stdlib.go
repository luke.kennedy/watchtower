package watchtower

import (
	"io/fs"
	"io/ioutil"
	"net"
	"os"
	"os/signal"
	"time"
)

var _ StdLib = StdlibProvider{}

// StdLib provides stdlib dependencies used directly by watchtower or commonly used in other service dependencies, including configuration loading.
// You are welcome to extend this in your own libraries, which is why it's used as a type constraint rather than directly as an interface.
type StdLib interface {
	fs.ReadFileFS
	fs.ReadDirFS
	// Listen creates a network listener
	Listen(network string, address string) (net.Listener, error)
	// Getenv gets an environment variable
	Getenv(k string) (v string)
	// Setenv sets an environment variable
	Setenv(k, v string) error
	// Now gets the current time
	Now() time.Time
	// Notify listens for OS signals
	Notify(c chan<- os.Signal, sig ...os.Signal)
	// SignalReset resets notifiers for the provided signals
	SignalReset(sig ...os.Signal)
}

// StdlibProvider implements StdLib using the stdlib, and should be used in all production watchtower services
type StdlibProvider struct{}

func (sp StdlibProvider) Listen(network string, address string) (net.Listener, error) {
	return net.Listen(network, address)
}

func (sp StdlibProvider) Getenv(k string) string {
	return os.Getenv(k)
}

func (sp StdlibProvider) Setenv(k, v string) error {
	return os.Setenv(k, v)
}

func (sp StdlibProvider) Open(name string) (fs.File, error) {
	return os.Open(name)
}

func (sp StdlibProvider) ReadFile(name string) ([]byte, error) {
	return ioutil.ReadFile(name)
}

func (sp StdlibProvider) ReadDir(name string) (out []fs.DirEntry, err error) {
	var entries []fs.FileInfo
	entries, err = ioutil.ReadDir(name)
	for _, e := range entries {
		out = append(out, infoWrapper{e})
	}
	return
}

func (sp StdlibProvider) Now() time.Time {
	return time.Now()
}

func (sp StdlibProvider) Notify(c chan<- os.Signal, sig ...os.Signal) {
	signal.Notify(c, sig...)
}

func (sp StdlibProvider) SignalReset(sig ...os.Signal) {
	signal.Reset(sig...)
}

var _ fs.DirEntry = infoWrapper{}

type infoWrapper struct {
	raw fs.FileInfo
}

func (iw infoWrapper) Name() string {
	return iw.raw.Name()
}

func (iw infoWrapper) IsDir() bool {
	return iw.raw.IsDir()
}

func (iw infoWrapper) Type() fs.FileMode {
	return iw.raw.Mode().Type()
}

func (iw infoWrapper) Info() (fs.FileInfo, error) {
	return iw.raw, nil
}
