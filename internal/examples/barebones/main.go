package main

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/luke.kennedy/watchtower"
)

/*
This is the most barebones example service, it has no dependencies, no modules, and uses no special logger.

It effectively starts an HTTP server on a random port with no valid routes, logging all attempted inbound traffic.
*/

type Config struct {
	watchtower.BaseConfig
}

func LoadConfig(ctx context.Context, stdlib watchtower.StdLib) (Config, error) {
	return Config{
		BaseConfig: watchtower.GetDefaultConfig(),
	}, nil
}

func main() {
	// Define log levels, this would normally be done in your logging package
	infoLevel := watchtower.SimpleLogLevelsDefinition.InfoLevel
	errorLevel := watchtower.SimpleLogLevelsDefinition.ErrorLevel
	// Create contexts for init and ongoing processes
	baseCtx := context.Background()
	initCtx, initCancel := context.WithTimeout(baseCtx, time.Minute)
	defer initCancel()
	// Create logger
	logger := watchtower.SimpleLogger[string]{}
	// Instantiate service wrapper
	s := watchtower.Service[Config, watchtower.NoDeps[Config, watchtower.StdLib], watchtower.StdLib, string]{
		StdLib:       watchtower.StdlibProvider{},
		CfgFactory:   watchtower.SimpleConfigFactory[Config, watchtower.StdLib]{SimpleLoader: LoadConfig},
		Logger:       logger,
		DepsFactory:  watchtower.NoDeps[Config, watchtower.StdLib]{},
		LoggerLevels: &watchtower.SimpleLogLevelsDefinition,
	}
	// Run init and check for error
	err := s.Init(initCtx, baseCtx)
	if err != nil {
		logger.Log(baseCtx, errorLevel, fmt.Sprintf("Init: %v", err))
	}
	// Run start and check for error
	ready, errs := s.Start(initCtx)
	select {
	case <-ready:
		logger.Log(baseCtx, infoLevel, "Service started without error!")
		select {}
	case err = <-errs:
		logger.Log(baseCtx, errorLevel, fmt.Sprintf("Start: %v", err))
	}
}
