package watchtower

import (
	"context"
	"log"
)

type Logger[TLevel any] interface {
	Log(ctx context.Context, level TLevel, message string)
}

type LoggerLevels[TLevel any] struct {
	InfoLevel  TLevel
	ErrorLevel TLevel
}

type SimpleLogLevels = LoggerLevels[string]

var SimpleLogLevelsDefinition = SimpleLogLevels{
	InfoLevel:  "info",
	ErrorLevel: "error",
}

type SimpleLogger[TLevel any] struct{}

func (l SimpleLogger[TLevel]) Log(ctx context.Context, level TLevel, message string) {
	log.Println(message)
}
