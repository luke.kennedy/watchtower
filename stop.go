package watchtower

import (
	"context"
	"fmt"
	"os"
	"sync"

	"golang.org/x/sync/errgroup"
)

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) listenForStop(ctx context.Context) {
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Setting up listener for sigterm...")
	signals := make(chan os.Signal, signalBufferSize)
	s.StdLib.Notify(signals, os.Interrupt)
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Waiting for sigterm or context ending...")
	select {
	case <-ctx.Done():
		// Whole app is stopping, start graceful shutdown if it isn't already
		s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Ongoing context ended! %v", ctx.Err()))
	case <-signals:
		// We have an interrupt, start graceful shutdown if it isn't already
		s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, "Received sigterm!")
	}
	s.StdLib.SignalReset(os.Interrupt)
	err := s.Stop(ctx)
	// TODO: maybe mock os.Exit, though that causes its own problems in testing
	if err != nil {
		s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Shutdown: %v", err))
		os.Exit(1)
	}
	os.Exit(0)
}

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) Stop(ctx context.Context) error {
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Attempting to shut down...")
	s.mx.Lock()
	s.st.ctx = ctx
	s.mx.Unlock()
	s.once.Do(s.st.op)
	return s.st.err
}

// This exists to avoid passing func literals into once.Do, just because I don't like that pattern very much in debugging
type stopper[TConfig Config, TDeps DependencyStack[TConfig, TStdLib], TStdLib StdLib, TLevel any] struct {
	s   *Service[TConfig, TDeps, TStdLib, TLevel]
	ctx context.Context
	err error
}

func (s *stopper[TConfig, TDeps, TStdLib, TLevel]) op() {
	s.err = s.s.stop(s.ctx)
}

// The actual stop logic, finally.
func (s *Service[TConfig, TDeps, TStdLib, TLevel]) stop(ctx context.Context) error {
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Shutting down...")
	eg := errgroup.Group{}
	eg.Go(func() error {
		err := s.server.Shutdown(ctx)
		if err != nil {
			err = fmt.Errorf("failed to shut down HTTP server: %w", err)
		}
		return err
	})
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Requesting module shutdowns...")
	for i, module := range s.HTTPModules {
		m := module
		i := i
		eg.Go(func() error {
			err := m.Stop(ctx)
			if err != nil {
				err = fmt.Errorf("failed to stop HTTP module %d: %w", i, err)
			}
			return err
		})
	}
	for i, module := range s.OtherModules {
		m := module
		i := i
		eg.Go(func() error {
			err := m.Stop(ctx)
			if err != nil {
				err = fmt.Errorf("failed to stop other module %d: %w", i, err)
			}
			return err
		})
	}
	errCh := make(chan error, 1)
	go func() {
		errCh <- eg.Wait()
	}()
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Waiting on modules shutting down...")
	select {
	case <-ctx.Done():
		return fmt.Errorf("context ended before service stopped: %w", ctx.Err())
	case err := <-errCh:
		if err != nil {
			return fmt.Errorf("failed to stop service module(s): %w", err)
		}
	}
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Shutdown complete")
	return nil
}

type cancelWrapper struct {
	ctx context.Context
	sync.WaitGroup
}

func withCancel(ctx context.Context) *cancelWrapper {
	cw := &cancelWrapper{}
	cw.Add(1)
	ctx2, cancel := context.WithCancel(ctx)
	go func(cancel func()) {
		defer func() { recover() }()
		cw.Wait()
		cancel()
	}(cancel)
	cw.ctx = ctx2
	return cw
}
