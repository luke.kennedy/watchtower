package watchtower

import (
	"context"
	"crypto/tls"
	"fmt"
	"runtime/debug"
)

// Config is the configuration used by watchtower
type Config interface {
	GetAddress() string
	GetPort() int
	// Return true to listen only on HTTP
	GetHTTPInsecure() bool
	// GetTLSConfig may only be called if GetHTTPInsecure returns false
	GetTLSConfig() *tls.Config
}

// BaseConfig satisfies the configuration required by watchtower, may be composed in larger config structs for simple implementation
type BaseConfig struct {
	Address      string `json:"address"`
	Port         int    `json:"port"`
	HTTPInsecure bool   `json:"httpInsecure"`
	TLSConfig    *tls.Config
}

func GetDefaultConfig() BaseConfig {
	return BaseConfig{
		Address:      "0.0.0.0",
		Port:         0,
		HTTPInsecure: true,
	}
}

func (b BaseConfig) GetAddress() string {
	return b.Address
}

func (b BaseConfig) GetPort() int {
	return b.Port
}

func (b BaseConfig) GetHTTPInsecure() bool {
	return b.HTTPInsecure
}

func (b BaseConfig) GetTLSConfig() *tls.Config {
	return b.TLSConfig
}

type ConfigFactory[TConfig Config, TStdLib StdLib] interface {
	// Load configuration, optionally using network access.
	LoadConfig(ctx context.Context, stdlib TStdLib) (TConfig, error)
	// CanHotLoad indicates whether this config factory supports hotloading.
	CanHotLoad() bool
	// HotLoad continuously or periodically reloads configuration, only usable if CanHotLoad returns true.
	HotLoad(ctx context.Context, stdlib StdLib) (updates <-chan TConfig, errs <-chan error)
}

type SimpleConfigFactory[TConfig Config, TStdLib StdLib] struct {
	SimpleLoader func(ctx context.Context, stdlib TStdLib) (TConfig, error)
}

// Load configuration, optionally using network access.
func (cf SimpleConfigFactory[TConfig, TStdLib]) LoadConfig(ctx context.Context, stdlib TStdLib) (TConfig, error) {
	return cf.SimpleLoader(ctx, stdlib)
}

// CanHotLoad indicates whether this config factory supports hotloading.
func (cf SimpleConfigFactory[TConfig, TStdLib]) CanHotLoad() bool {
	return false
}

// HotLoad continuously or periodically reloads configuration, only usable if CanHotLoad returns true.
func (cf SimpleConfigFactory[TConfig, TStdLib]) HotLoad(ctx context.Context, stdlib StdLib) (updates <-chan TConfig, errs <-chan error) {
	panic("simple config factory cannot be hot loaded")
}

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) loadConfig(initCtx, ongoingCtx context.Context) (cfg TConfig, err error) {
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Loading configuration...")
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Validating service state...")
	if s == nil {
		return cfg, fmt.Errorf("cannot operate nil service")
	}
	if initCtx == nil {
		return cfg, fmt.Errorf("cannot load config with nil context")
	}
	if err = initCtx.Err(); err != nil {
		return cfg, fmt.Errorf("cannot load config due to context: %w", err)
	}
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Service state validated")
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Executing configuration factory...")
	cfg, err = s.CfgFactory.LoadConfig(initCtx, s.StdLib)
	if err != nil {
		return cfg, fmt.Errorf("failed to load initial config: %w", err)
	}
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Initialising dependency stack...")
	err = s.DepsFactory.Init(initCtx, cfg, s.StdLib)
	if err != nil {
		return cfg, fmt.Errorf("failed to initialise dependency stack: %w", err)
	}
	cfgCanHotLoad := s.CfgFactory.CanHotLoad()
	var hotLoadable []Module[TConfig, TDeps, TStdLib]
	// TODO: parallelise this
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Initialising modules...")
	for i, module := range s.HTTPModules {
		err = module.Init(initCtx, ongoingCtx, cfg, s.DepsFactory, s.StdLib)
		if err != nil {
			return cfg, fmt.Errorf("failed to initialise HTTP module %d: %w", i, err)
		}
		if cfgCanHotLoad && module.CanHotLoad() {
			hotLoadable = append(hotLoadable, module)
		}
	}
	for i, module := range s.OtherModules {
		err = module.Init(initCtx, ongoingCtx, cfg, s.DepsFactory, s.StdLib)
		if err != nil {
			return cfg, fmt.Errorf("failed to initialise other module %d: %w", i, err)
		}
		if cfgCanHotLoad && module.CanHotLoad() {
			hotLoadable = append(hotLoadable, module)
		}
	}
	if cfgCanHotLoad {
		s.hotLoad(hotLoadable)
	}
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Configuration loaded")
	return cfg, nil
}

// hotLoad begins hot-loading config and passing the new values into the modules. It does not return anything, and only terminates when s.ongoing.ctx is marked as done, or if the factory's updates channel closes.
func (s *Service[TConfig, TDeps, TStdLib, TLevel]) hotLoad(hotLoadable []Module[TConfig, TDeps, TStdLib]) {
	ctx := s.ongoing.ctx
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Initialising hotloading...")
	update := make(chan TConfig, updateBufferSize)
	go func(update chan<- TConfig) {
		defer func() {
			rErr := recover()
			close(update)
			if rErr != nil {
				ctx := context.Background()
				s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Caught panic in service startup: %v", rErr))
				s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Panic trace:\n%s", debug.Stack()))
			}
		}()
		updates, errs := s.CfgFactory.HotLoad(ctx, s.StdLib)
	HOT_LOAD_WRITE_LOOP:
		for {
			select {
			case next, open := <-updates:
				update <- next
				s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Received configuration hotload update")
				if !open {
					break HOT_LOAD_WRITE_LOOP
				}
			case err := <-errs:
				s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Error during configuration hot-load: %v", err))
			case <-ctx.Done():
				// Long-running processes need to stop
				break HOT_LOAD_WRITE_LOOP
			}
		}
	}(update)
	go func(update <-chan TConfig) {
		defer func() {
			rErr := recover()
			if rErr != nil {
				ctx := context.Background()
				s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Caught panic in service startup: %v", rErr))
				s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Panic trace:\n%s", debug.Stack()))
			}
		}()
		outPipes := make([]chan<- TConfig, 0, len(hotLoadable))
		for _, module := range hotLoadable {
			outPipe := make(chan TConfig, updateBufferSize)
			outPipes = append(outPipes, outPipe)
			module.HotLoad(ctx, outPipe)
		}
	HOT_LOAD_READ_LOOP:
		for {
			select {
			case next, open := <-update:
				for _, pipe := range outPipes {
					pipe <- next
					if !open {
						close(pipe)
					}
				}
				if !open {
					break HOT_LOAD_READ_LOOP
				}
			case <-ctx.Done():
				// Long-running processes need to stop
				for _, pipe := range outPipes {
					close(pipe)
				}
				break HOT_LOAD_READ_LOOP
			}
		}
	}(update)
}
