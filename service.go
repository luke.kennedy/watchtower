package watchtower

import (
	"context"
	"crypto/tls"
	"fmt"
	"net/http"
	"regexp"
	"runtime/debug"
	"sync"
	"time"
)

const (
	updateBufferSize = 10
	signalBufferSize = 100
)

type Module[TConfig any, TDeps DependencyStack[TConfig, TStdLib], TStdLib StdLib] interface {
	// Init provides loaded configuration and dependencies, performing synchronous initialisation.
	//
	// initCtx controls the processes for loading configuration and dependencies, ongoingCtx is used to control service lifetime. If ongoingCtx.Err() returns non-nil, module must begin graceful shutdown using Stop
	Init(initCtx, ongoingCtx context.Context, cfg TConfig, deps TDeps, stdlib TStdLib) error
	// Start performs asynchronous startup, ready and err must only write once each in that order
	Start(ctx context.Context) (ready <-chan struct{}, err <-chan error)
	// Stop attempts to gracefully stop the module within the time allowed by ctx. Must be idempotent, including when triggered internally by ongoingCtx expiry/cancellation.
	Stop(ctx context.Context) error
	// CanHotLoad indicates whether this module can accept hot-loaded configuration
	CanHotLoad() bool
	// HotLoad provides newly hot-loaded configuration, will only be called if CanHotLoad returns true and config factory also supports hot-loading. updates *must* be read and unblocked in a timely manner.
	HotLoad(ctx context.Context, updates <-chan TConfig)
}

// Service is a watchtower service
type Service[TConfig Config, TDeps DependencyStack[TConfig, TStdLib], TStdLib StdLib, TLevel any] struct {
	StdLib      TStdLib
	CfgFactory  ConfigFactory[TConfig, TStdLib]
	DepsFactory TDeps
	// HTTPModules should be singletons ready to initialise using the Init method
	HTTPModules []HTTPModule[TConfig, TDeps, TStdLib]
	// OtherModules should be singletons ready to initialise using the Init method
	OtherModules []Module[TConfig, TDeps, TStdLib]
	// Logger is only used by the service framework and is not passed down.
	//
	// If you wish to use the same logger in modules, inject it to the modules before setting them on Service, or inject it into Deps
	Logger       Logger[TLevel]
	LoggerLevels *LoggerLevels[TLevel]
	// Adding a path to this list will exclude it from HTTP path auto-logging
	UnloggedPaths       []string
	UnloggedPathsRegexp []*regexp.Regexp
	// TopLevelMiddleware is the HTTP middleware which will run first when receiving a new request, before passing on to auto-logging and module-internal middleware.
	//
	// Optional, nil will simply not run this layer of middleware.
	TopLevelMiddleware HTTPMiddleware
	server             *http.Server
	addr               string
	// Used for long-running processes not expected to terminate with the startup context
	ongoing *cancelWrapper
	once    sync.Once
	mx      sync.Mutex
	st      stopper[TConfig, TDeps, TStdLib, TLevel]
	mux     *http.ServeMux
}

// Init initialises the services and dependencies
func (s *Service[TConfig, TDeps, TStdLib, TLevel]) Init(initCtx context.Context, ongoingCtx context.Context) error {
	if s == nil {
		return fmt.Errorf("cannot operate nil service")
	}
	if initCtx == nil {
		return fmt.Errorf("cannot initialise with nil context")
	}
	if s.CfgFactory == nil {
		return fmt.Errorf("cannot use nil config factory")
	}
	if s.LoggerLevels == nil || s.Logger == nil {
		s.mx.Lock()
		s.Logger = SimpleLogger[TLevel]{}
		// We know this will be ignored, so it's fine that these are possibly invalid defaults
		var defaultLevel TLevel
		s.LoggerLevels = &LoggerLevels[TLevel]{
			InfoLevel:  defaultLevel,
			ErrorLevel: defaultLevel,
		}
		s.Logger.Log(initCtx, s.LoggerLevels.ErrorLevel, "Missing logging configuration in watchtower.Service.Init, must provide both Logger and LoggerLevels")
		s.mx.Unlock()
	}
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "Saving ongoing context...")
	s.mx.Lock()
	s.ongoing = withCancel(ongoingCtx)
	s.st = stopper[TConfig, TDeps, TStdLib, TLevel]{s: s}
	s.mx.Unlock()
	cfg, err := s.loadConfig(initCtx, s.ongoing.ctx)
	if err != nil {
		return fmt.Errorf("initialising and configuring: %w", err)
	}
	s.configureHTTP(initCtx, cfg)
	s.Logger.Log(initCtx, s.LoggerLevels.InfoLevel, "watchtower service initialisation complete")
	return nil
}

// Start performs asynchronous startup, ready and err will only write once each in that order, or only err will write.
//
// ctx is only used prior to the write to ready, after that ongoingCtx from Init is used instead.
func (s *Service[TConfig, TDeps, TStdLib, TLevel]) Start(ctx context.Context) (<-chan struct{}, <-chan error) {
	ready := make(chan struct{}, 1)
	err := make(chan error, 1)
	go s.start(ctx, ready, err)
	go s.listenForStop(s.ongoing.ctx)
	return ready, err
}

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) start(ctx context.Context, ready chan<- struct{}, err chan<- error) {
	defer func() {
		r := recover()
		close(err)
		close(ready)
		if r != nil {
			s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Caught panic in service startup: %v", r))
			s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Panic trace:\n%s", debug.Stack()))
		}
	}()
	s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Starting modules...")
	r, e := s.DepsFactory.Start(ctx)
	select {
	case <-ctx.Done():
		// Ran out of time, abort
		err <- fmt.Errorf("failed to start dependencies before context ended: %w", ctx.Err())
		return
	case <-r:
		// Deps are ready, we can start modules
	case depErr, _ := <-e:
		// Deps failed, abort
		err <- fmt.Errorf("failed to start dependencies: %w", depErr)
		return
	}
	// +1 is for the HTTP server, which we can start in parallel with non-HTTP modules
	numFirstStartThreads := len(s.OtherModules) + 1
	wg := sync.WaitGroup{}
	wg.Add(numFirstStartThreads)
	allDone := make(chan struct{}, 1)
	go func() {
		wg.Wait()
		allDone <- struct{}{}
		close(allDone)
	}()
	errs := make(chan error, numFirstStartThreads)
	for i, module := range s.OtherModules {
		go s.startModule(ctx, i, module, &wg, errs)
	}
	go func() {
		defer func() {
			rErr := recover()
			wg.Done()
			if rErr != nil {
				s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Caught panic in service startup: %v", rErr))
				s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Panic trace:\n%s", debug.Stack()))
			}
		}()
		s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Starting network listener...")
		l, err := s.StdLib.Listen("tcp", s.addr)
		if err != nil {
			errs <- fmt.Errorf("could not start listener: %w", err)
			return
		}
		s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Started network listener on %s", l.Addr().String()))
		if s.server.TLSConfig != nil {
			l = tls.NewListener(l, s.server.TLSConfig)
		}
		go func() {
			s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, "Starting HTTP server...")
			errs <- s.server.Serve(l)
		}()
		// TODO: wait for HTTP server to be ready
		r := make(chan struct{}, 1)
		go func() {
			time.Sleep(10 * time.Second)
			r <- struct{}{} // dummy code
		}()
		select {
		case <-r:
			return // Done
		case <-ctx.Done():
			errs <- fmt.Errorf("failed to start HTTP server before context ended: %w", ctx.Err())
		}
	}()

	select {
	case <-ctx.Done():
		// Ran out of time, abort
		err <- fmt.Errorf("failed to start non-HTTP modules & HTTP server before context ended: %w", ctx.Err())
		return
	case startErr, _ := <-errs:
		err <- fmt.Errorf("failed to start non-HTTP modules & HTTP server: %w", startErr)
		return
	case <-allDone:
		// No error before all ready, we can now start HTTP modules
	}

	numHTTPStartThreads := len(s.HTTPModules)
	wg2 := sync.WaitGroup{}
	wg2.Add(numHTTPStartThreads)
	allDone2 := make(chan struct{}, 1)
	go func() {
		wg2.Wait()
		allDone2 <- struct{}{}
		close(allDone2)
	}()
	errs2 := make(chan error, numHTTPStartThreads)
	for i, module := range s.OtherModules {
		go s.startModule(ctx, i, module, &wg2, errs2)
	}

	select {
	case <-ctx.Done():
		// Ran out of time, abort
		err <- fmt.Errorf("failed to start HTTP modules before context ended: %w", ctx.Err())
	case startErr, _ := <-errs2:
		err <- fmt.Errorf("failed to start HTTP modules: %w", startErr)
	case <-allDone2:
		// All done for real
		ready <- struct{}{}
	}
	// Hang until service dies or a module dies
	select {
	case <-s.ongoing.ctx.Done():
		// Ran out of time, abort
		err <- fmt.Errorf("long-running context ended before error arrived from running modules: %w", ctx.Err())
	case runtimeErr, _ := <-errs:
		err <- fmt.Errorf("error occurred in running module: %w", runtimeErr)
	case runtimeErr, _ := <-errs2:
		err <- fmt.Errorf("error occurred in running module: %w", runtimeErr)
	}
}

func (s *Service[TConfig, TDeps, TStdLib, TLevel]) startModule(ctx context.Context, i int, module Module[TConfig, TDeps, TStdLib], wg *sync.WaitGroup, errs chan<- error) {
	defer func() {
		rErr := recover()
		wg.Done()
		if rErr != nil {
			s.Logger.Log(ctx, s.LoggerLevels.ErrorLevel, fmt.Sprintf("Caught panic in service startup: %v", rErr))
			s.Logger.Log(ctx, s.LoggerLevels.InfoLevel, fmt.Sprintf("Panic trace:\n%s", debug.Stack()))
		}
	}()
	r, e := module.Start(ctx)
	go func() {
		// We want to keep error listening going after ready checks
		defer recover()
		err, _ := <-e
		errs <- err
	}()
	select {
	case <-r:
		return // Done
	case <-ctx.Done():
		errs <- fmt.Errorf("failed to start module %d before context ended: %w", i, ctx.Err())
	}
}
