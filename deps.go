package watchtower

import (
	"context"
)

// DependencyStack is a general-purpose dependency stack initialiser.
type DependencyStack[TCfg any, TStdLib StdLib] interface {
	// Run synchronous startup logic.
	Init(ctx context.Context, cfg TCfg, stdlib TStdLib) error
	// Run asynchronous startup logic. Ready may only write once, errs may write indefinitely.
	Start(ctx context.Context) (ready <-chan struct{}, errs <-chan error)
	// Stop, synchronously or asynchronously according to internal logic. Error channel may only write once.
	Stop(ctx context.Context) <-chan error
}

type NoDeps[TConfig any, TStdLib StdLib] struct{}

// Run synchronous startup logic.
func (d NoDeps[TConfig, TStdLib]) Init(ctx context.Context, cfg TConfig, stdlib TStdLib) error {
	return nil
}

// Run asynchronous startup logic. Ready may only write once, errs may write indefinitely.
func (d NoDeps[TConfig, TStdLib]) Start(ctx context.Context) (ready <-chan struct{}, errs <-chan error) {
	readyRaw := make(chan struct{}, 1)
	readyRaw <- struct{}{}
	close(readyRaw)
	ready = readyRaw
	errs = make(<-chan error) // Technically this is never closed, but it's a very small leak
	return
}

// Stop, synchronously or asynchronously according to internal logic. Error channel may only write once.
func (d NoDeps[TConfig, TStdLib]) Stop(ctx context.Context) <-chan error {
	errs := make(chan error, 1)
	errs <- nil
	close(errs)
	return errs
}
